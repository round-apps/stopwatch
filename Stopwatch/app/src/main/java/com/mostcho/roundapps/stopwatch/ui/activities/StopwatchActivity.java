package com.mostcho.roundapps.stopwatch.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mostcho.roundapps.stopwatch.R;

public class StopwatchActivity extends AppCompatActivity {
    // ---------------------------------------------------------------------------------------------
    // Constants
    // ---------------------------------------------------------------------------------------------
    private static final String TAG = StopwatchActivity.class.getSimpleName();
    private static final int HANDLER_DELAY_INTERVAL = 0;

    // ---------------------------------------------------------------------------------------------
    // Fields methods
    // ---------------------------------------------------------------------------------------------
    private TextView tvChronometer;
    private TextView tvCurrentSplitTime;
    private LinearLayout llSplitTimes;
    private TextView tvSplitTime;

    private boolean isChronometerRunning;
    private boolean isCurrentSplitTimeVisible;

    private long uptimeMillis;

    private long totalStartTime;
    private long totalTimeMillis;
    private long totalTimeSwapBuff;
    private long totalUpdatedTime;
    private int totalMinutes;
    private int totalSeconds;
    private int totalMillis;

    private long splitStartTime;
    private long splitTimeMillis;
    private long splitTimeSwapBuff;
    private long splitUpdatedTime;
    private int splitMinutes;
    private int splitSeconds;
    private int splitMillis;

    private int splitCount = 1;

    private Handler handler;
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            uptimeMillis = SystemClock.uptimeMillis();
            //total time
            totalTimeMillis = uptimeMillis - totalStartTime;
            totalUpdatedTime = totalTimeSwapBuff + totalTimeMillis;
            totalSeconds = (int) (totalUpdatedTime / 1000);
            totalMinutes = totalSeconds / 60;
            totalSeconds = totalSeconds % 60;
            totalMinutes = totalMinutes % 60;
            totalMillis = (int) (totalUpdatedTime % 100);

            //split time
            splitTimeMillis = uptimeMillis - splitStartTime;
            splitUpdatedTime = splitTimeSwapBuff + splitTimeMillis;
            splitSeconds = (int) (splitUpdatedTime / 1000);
            splitMinutes = splitSeconds / 60;
            splitSeconds = splitSeconds % 60;
            splitMinutes = splitMinutes % 60;
            splitMillis = (int) (splitUpdatedTime % 100);

            tvChronometer.setText(getString(R.string.timer_value, totalMinutes, totalSeconds, totalMillis));

            if (isCurrentSplitTimeVisible) {
                tvCurrentSplitTime.setText(getString(R.string.split_time_value,
                    splitCount, splitMinutes, splitSeconds, splitMillis, totalMinutes, totalSeconds, totalMillis));
            }

            handler.postDelayed(this, HANDLER_DELAY_INTERVAL);
        }
    };

    // ---------------------------------------------------------------------------------------------
    // Activity lifecycle
    // ---------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stopwatch);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        isCurrentSplitTimeVisible = false;
        isChronometerRunning = false;
        handler = new Handler();

        findViewById(R.id.tv_split).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChronometerRunning) {
                    tvSplitTime =
                        (TextView) LayoutInflater.from(StopwatchActivity.this).inflate(R.layout.list_item_split_time, null, false);
                    tvSplitTime.setText(getString(R.string.split_time_value,
                        splitCount, splitMinutes, splitSeconds, splitMillis, totalMinutes, totalSeconds, totalMillis));
                    llSplitTimes.addView(tvSplitTime, 0);
                    if (splitCount == 1) {
                        tvCurrentSplitTime.setVisibility(View.VISIBLE);
                        isCurrentSplitTimeVisible = true;
                    }
                    splitCount++;
                    resetSplitTimes();
                    splitStartTime = uptimeMillis;
                    Log.d(TAG, "Split: " + tvSplitTime.getText());
                }
            }
        });


        tvChronometer = (TextView) findViewById(R.id.chronometer);
        tvChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChronometerRunning) {
                    totalTimeSwapBuff += totalTimeMillis;
                    splitTimeSwapBuff += splitTimeMillis;
                    handler.removeCallbacks(updateTimerThread);
                    isChronometerRunning = false;
                } else {
                    splitStartTime = totalStartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(updateTimerThread, HANDLER_DELAY_INTERVAL);
                    isChronometerRunning = true;
                }
            }
        });
        tvChronometer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!isChronometerRunning) {
                    resetChronometer();
                    Toast.makeText(StopwatchActivity.this, "reset", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        tvCurrentSplitTime = (TextView) findViewById(R.id.tv_current_split_time);
        llSplitTimes = (LinearLayout) findViewById(R.id.ll_split_times);

        resetChronometer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null && updateTimerThread != null) {
            handler.removeCallbacks(updateTimerThread);
        }
        resetChronometer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // ---------------------------------------------------------------------------------------------
    // Private methods
    // ---------------------------------------------------------------------------------------------
    private void resetChronometer() {
        uptimeMillis = 0L;
        totalStartTime = 0L;
        totalTimeSwapBuff = 0L;
        totalUpdatedTime = 0L;
        totalMinutes = 0;
        totalSeconds = 0;
        totalMillis = 0;
        tvChronometer.setText(getString(R.string.timer_value, totalMinutes, totalSeconds, totalMillis));
        isChronometerRunning = false;
        resetSplitTimes();
        splitCount = 1;
        tvCurrentSplitTime.setText(R.string.split_time_initial_value);
        tvCurrentSplitTime.setVisibility(View.INVISIBLE);
        llSplitTimes.removeAllViews();
    }

    private void resetSplitTimes() {
        splitStartTime = 0L;
        splitTimeSwapBuff = 0L;
        splitUpdatedTime = 0L;
        splitMinutes = 0;
        splitSeconds = 0;
        splitMillis = 0;
    }
}
